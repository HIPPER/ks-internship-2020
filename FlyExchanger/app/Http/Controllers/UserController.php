<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request) {

        $this->validate($request, [
            'username' => 'required|email|unique:users',
            'password_hash' => 'required|confirmed',
            'api_token' => 'required|unique:users'
            //'created_at' => 'required|dateTime'
        ]);

        $input = $request->only('username', 'password_hash', 'api_token');

        try {

            $user = new User;
            $user->username = $input['username'];
            $password = $input['password_hash'];
            $user->password_hash = app('hash')->make($password);
            $user->api_token = $input['api_token'];
            //$user->created_at = $request->input('created_at');

            if($user->save()) {
                $code = 200;
                $output = [
                    'user' => $user,
                    'code' => $code,
                    'message' => 'User created successfully.'
                ];

            } else {
                $code = 500;
                $output = [
                    'code' => $code,
                    'message' => 'An error occurred while creating user.'
                ];

            }

        } catch (Exception $e){
            // dd($e->getMessage());

            $code = 500;
            $output = [
                'code' => $code,
                'message' => 'An error occurred while creating user.'
            ];
        }

        return response()->json($output, $code);

    }


}
