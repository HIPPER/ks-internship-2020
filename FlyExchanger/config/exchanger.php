<?php

return [
    'file_min_lifetime_sec' => env('FILE_MIN_LT_SEC'),
    'file_max_lifetime_sec' => env('FILE_MAX_LT_SEC'),
    'file_max_size_kb' => env('FILE_MAX_SIZE'),
    'confirmed_account_max_size' => env(),
    'not_confirmed_account_max_size' => env(),
];
