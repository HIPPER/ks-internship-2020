<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitesSharedDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invites_shared_directories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invite_id')->unsigned();
            $table->foreign('invite_id')->references('id')->on('invites')->onDelete('cascade');
            $table->integer('directory_id')->unsigned();
            $table->foreign('directory_id')->references('id')->on('directories')->onDelete('cascade');
            $table->unique(['invite_id', 'directory_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invites_shared_directories', function (Blueprint $table) {
            $table->dropForeign('invites_shared_directories_invite_id_foreign');
            $table->dropForeign('invites_shared_directories_directory_id_foreign');
        });
        Schema::dropIfExists('invites_shared_directories');
    }
}
