<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_directories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('directory_id')->unsigned();
            $table->foreign('directory_id')->references('id')->on('directories')->onDelete('cascade');
            $table->boolean('is_master');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_directories', function (Blueprint $table) {
            $table->dropForeign('users_directories_user_id_foreign');
            $table->dropForeign('users_directories_directory_id_foreign');
        });
        Schema::dropIfExists('users_directories');
    }
}
