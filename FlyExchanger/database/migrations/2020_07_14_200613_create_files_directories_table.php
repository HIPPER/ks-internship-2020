<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesDirectoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_directories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_id')->unsigned();
            $table->foreign('file_id')->references('id')->on('files')->onDelete('cascade');
            $table->integer('directory_id')->unsigned();
            $table->foreign('directory_id')->references('id')->on('directories')->onDelete('cascade');
            $table->unique(['directory_id', 'file_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('files_directories', function (Blueprint $table) {
            $table->dropForeign('files_directories_file_id_foreign');
            $table->dropForeign('files_directories_directory_id_foreign');
        });
        Schema::dropIfExists('files_directories');
    }
}
